<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreService" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.Entity" %>
<%@ page import="com.google.appengine.api.datastore.FetchOptions" %>
<%@ page import="com.google.appengine.api.datastore.Key" %>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>
<%@ page import="com.google.appengine.api.datastore.Query" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>TweetFB By Junuo</title>
    <meta name="author" content="Junuo Cai">
    
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	

    <link href="./css/bootstrap.min.css" media="all" type="text/css" rel="stylesheet">
    <link href="./css/bootstrap-responsive.min.css" media="all" type="text/css" rel="stylesheet">
    <link href="./css/font-awesome.css" rel="stylesheet" >
    <link href="./css/nav-fix.css" media="all" type="text/css" rel="stylesheet">

   

  </head>
<body>
 <div id="fb-root"></div>
<script>
// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
console.log('statusChangeCallback');
console.log(response);
// The response object is returned with a status field that lets the
// app know the current login status of the person.
// Full docs on the response object can be found in the documentation
// for FB.getLoginStatus().
if (response.status === 'connected') {
// Logged into your app and Facebook.
var msg=document.getElementById('main_tweet');
msg.style.display='';
var login_div=document.getElementById('status');
login_div.style.display='none';
testAPI();
} 
else if (response.status === 'not_authorized') {
// The person is logged into Facebook, but not your app.
var msg=document.getElementById('main_tweet');
msg.style.display='none';
var profile=document.getElementById('profile_link');
profile.style.display='none';
document.cookie = "userid=" ;
document.cookie = "username=";


} else {
// The person is not logged into Facebook, so we're not sure if
// they are logged into this app or not.
var msg=document.getElementById('main_tweet');
msg.style.display='none';
var profile=document.getElementById('profile_link');
msg.style.display='none';
document.cookie = "userid=" ;
document.cookie = "username=";

}
}


var login_event = function(response) {
	var msg=document.getElementById('main_tweet');
	msg.style.display='';
	var login_div=document.getElementById('status');
	login_div.style.display='none';
}

var logout_event = function(response) {
	var msg=document.getElementById('main_tweet');
	msg.style.display='none';
	var profile=document.getElementById('profile_link');
	msg.style.display='none';
	var login_div=document.getElementById('status');
	login_div.style.display='';
	document.cookie = "userid=" ;
	document.cookie = "username=";

}
// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }


window.fbAsyncInit = function() {
    FB.init({
   	appId      : '383500985442807',
      cookie     : true,  // enable cookies to allow the server to access 
      // the session
      xfbml      : true,
      version    : 'v2.11'
    });  
FB.getLoginStatus(function(response) {
	    statusChangeCallback(response);
	  });
FB.Event.subscribe('auth.statusChange', function(response) {
    if (response.status === 'connected') {
                  //the user is logged and has granted permissions
       login_event();
    } else if (response.status === 'not_authorized') {
          //ask for permissions
         logout_event();
    } else {
    	logout_event();
          //ask the user to login to facebook
    }
});

	  };	  
(function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
   
   



var post = function() {
	var text = document.getElementById('tweet_text').value;
	console.log("value of text is " + text);
	FB.api('/me/feed', 'post', { message: text }, function(response) {
		  if (!response || response.error) {
		    alert('Error occured due to: ' + response.error.message);
		    console.log(response.error.message);
		  } else {
		    alert('Post ID: ' + response.id);
		    location.reload();
		  }
		});

	
}


// Here we run a very simple test of the Graph API after login is
// successful. See statusChangeCallback() for when this call is made.
function share() {
	var tweet_text = document.getElementById('tweet_text').value;
	var userid = document.getElementById('userid').value;
	var username = document.getElementById('username').value;
	var picture = document.getElementById('picture').value;
	var msg_tweet = "true";

	var post_data = {
			  tweet_text: tweet_text,
			  userid: userid  , 
			  username: username,
			 picture: picture,
			 msg_tweet : "true"
			};
	$.post( "Tweet", post_data, function(data) {
		console.log(data);
		var key = data;
		var url = window.location.href ;
		if (url.search("localhost")!==-1) {
			url = "https://facebook.com/";
		}
		var share_url = url + "view.jsp?tweet_key=" + key ;
		var dict = {
				  method: 'send',
				  link: share_url  , 
				  caption: 'tweet',
				 description: 'Dialogs provide a simple, consistent interface for applications to interface with users.'
				   
				};

	FB.ui(
				   dict ,
				   function(response) {

				     if (response && response.success) {
				       alert('Post was shared via message.');
				       location.reload();
				     } else {
				       alert('Post was not published due to .' + response.error.message);
				     }
				   }
				 );	
		
		
		
		

	} );
	
	


}
var profile_url = "";
function testAPI() {

console.log('Welcome! Fetching your information.... ');
FB.api('/me', function(response) {

console.log('Successful login for: ' + response.name);
console.log('response is ' + JSON.stringify(response));
document.getElementById('profile_pic').innerHTML = '<a href="#" class="thumbnail"><img src="http://graph.facebook.com/' + response.id + '/picture?type=large" /></a>';
document.getElementById('fullname').innerText = response.name;
document.getElementById('fullname_head').innerText = response.name;
document.getElementById('whatsup').innerText = 'Hello! ' + response.name;
document.getElementById('profile_link').href = 'https://facebook.com/' + response.id;
profile_url = 'https://facebook.com/' + response.id;
localStorage.profile_url = profile_url;
document.getElementById('picture').value = 'http://graph.facebook.com/' + response.id + '/picture';
document.getElementById('userid').value =  response.id;
document.getElementById('username').value =  response.name.split(" ")[0];
document.cookie = "userid=" + response.id;
document.cookie = "username=" + response.name.split(" ")[0];
document.cookie = "profile=" + "https://facebook.com/" + response.id;
document.cookie = "picture=" + "http://graph.facebook.com/" + response.id + "/picture?type=large";

});
}
</script>


<% Cookie[] cookies = request.getCookies();
		String userid="", username="",picture="";
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				Cookie cookie = cookies[i];
				if (cookie.getName().equals("userid")) {
					userid = cookie.getValue();
				}

				if (cookie.getName().equals("username")) {
					username = cookie.getValue();
				}

				if (cookie.getName().equals("picture")) {
					picture = cookie.getValue();
				}
			}
		}
		
		%>

    
    <div class="container">
	<header class="header clearfix">
        <nav>
          <ul class="nav nav-pills float-right">
            <li class="nav-item">
              <a class="nav-link active" href="#">Tweet <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="friends_tweet" href="./friends.jsp">Friends</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="friends_top_tweets" href="./top_tweets.jsp">Top Tweets</a>
            </li>
            <li class="nav-item">
            <fb:login-button size="large" autologoutlink="true" scope="public_profile,email" onlogin="checkLoginState();">
			</fb:login-button>
            </li >
          </ul>
        </nav>
        <h3 class="text-muted" href="#">Twitter on FaceBook</h3>
      </header>
      
      <main role="main">
        <div id="status" class="jumbotron">
          <h1 class="display-3">Welcome to my App</h1>
          <p class="lead">In this app, users can post the tweet on their facebook page and send a direct message to friends with the tweet they just made as a content.</p>
          <p><a class="btn btn-lg btn-success" href="#" role="button">Sign up today</a></p>
          <fb:login-button size="xlarge" autologoutlink="true" scope="public_profile,email" onlogin="checkLoginState();">
			</fb:login-button>
        </div>
      </main>
      
      </div>
      
    
    
    
<div id="main_tweet" class="container" style="display:none;">
<div class="row">
   <div class="span12">
    <p class="lead"> Hello! <strong><a id="fullname"> </a> </strong></p>
    
    <div class="row">
      <div class="span11 well">
      <form method="post" action="Tweet" name="post_tweet" id="post_tweet" accept-charset="UTF-8">        
      <input type="hidden" name="userid" id="userid" value=""/>
      <input type="hidden" name="username" id="username" value=""/>
      <input type="hidden" name="picture" id="picture" value=""/>                  
      <textarea class="span10 form-control" id="tweet_text" name="tweet_text" rows="5" placeholder="Type in your new tweet"></textarea>
        <input type="submit" name="post_btn" value="Post New Tweet" class="btn btn-info" onclick="post()"/>
        <input type="button" name="share_btn" value="Share with friends" class="btn btn-primary" onclick="share()"/>
        </form>     
         </div>
    </div>
    
    <div class="row">
      <div class="span11 well">
        <div class="row">
          <div id="profile_pic" class="span4"><a href="#" class="thumbnail"><img src="./img/user.jpg" alt=""></a></div>
          <div class="span3">
            <h3><a id="fullname_head"> </a></h3>
            <span id="num_tweets" class=" badge badge-warning">0 tweets</span>        
           </div>
        </div>
      </div>
    </div>

    <div class="container">
    <div class="row">
   <div class="span11 well" style="overflow-y: scroll; height:101%;">
        <p class="lead"> Tweets by <a id="fullname_head"> </a></p>
            		  <hr />
   
<%
    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    // Run an ancestor query to ensure we see the most up-to-date
    // view of the Greetings belonging to the selected Guestbook.
    Query query = new Query("Tweet").addSort("date", Query.SortDirection.DESCENDING);
    query.addFilter("userid",
            Query.FilterOperator.EQUAL,
            userid);
    List<Entity> tweets = datastore.prepare(query).asList(FetchOptions.Builder.withChunkSize(2000));
    int num_tweets = tweets.size();
    if (tweets.isEmpty()) {
%>
<div class="alert alert-danger"> <p> No tweets to be shown</p>
</div>
<%
}
else { 
%>
	<script type="text/javascript"> console.log(<%=num_tweets%>);document.getElementById("num_tweets").innerText = "<%=num_tweets%> tweets";</script>
<% 
	for (Entity tweet : tweets) { 
		String tweet_text =  (String) tweet.getProperty("text");
		String tweet_date = (String) tweet.getProperty("date");
		String user_name = (String) tweet.getProperty("username");
		String key = KeyFactory.keyToString(tweet.getKey());
		String href = "'view.jsp?tweet_key=" + key + "'";
%>
    		  <div>
            <a class="active" href=<%=href%> ><%=tweet_text%></a>
            <form method="post" action="Delete" name="delete_tweet" accept-charset="UTF-8">     
            <input type="hidden" name="tweet_key" id="tweet_key" value="<%=key%>"/>    
            <input type="submit" class="btn btn-dark pull-right" value="Delete"/>
            </form>
            <span class="badge pull-left">Creat By: <%=user_name%></span><hr />
            <span class="badge pull-left">Creat At: <%=tweet_date%></span>
            <p>&nbsp;</p>
    		</div>      
    		   <hr />
<% 
  } 
%>

        <ul class="pager"><li class="previous disabled"><a href="#">&laquo; Previous</a></li><li class="next disabled"><a href="#">Next &raquo;</a></li></ul>      
    </div>
    </div>
    </div>
    
    <div class="container">
    <div class="row">
      <div class="span11 well" style="overflow-y: scroll; height:101%;">
        <p class="lead"> Top Tweets of <em><a id="fullname"> </a></em>:</p>
           <hr />
        <%   Query c_query = new Query("Tweet").addSort("count", Query.SortDirection.DESCENDING);
c_query.addFilter("userid",Query.FilterOperator.EQUAL,userid);
List<Entity> c_tweets = datastore.prepare(c_query).asList(FetchOptions.Builder.withChunkSize(2000)); 
for (Entity each_tweet : c_tweets ) { 
		String c_tweet_text =  (String) each_tweet.getProperty("text");
		String c_tweet_date = (String) each_tweet.getProperty("date");
		Long c_tweet_count = (Long) each_tweet.getProperty("count");
		String c_key = KeyFactory.keyToString(each_tweet.getKey());
		String c_href = "'view.jsp?tweet_key=" + c_key + "'";%>
    		<div>
            <a class="active" href=<%=c_href%> ><%=c_tweet_text%></a>
            <p>&nbsp;</p>
            <button type="submit" class="btn btn-info pull-right">View Count <%=c_tweet_count %></button>
             <span class="badge pull-left"><%=c_tweet_date%></span>
            <p>&nbsp;</p>
    	   </div>  
    	    <hr />	
    	   <% } %>    
    	     
       </div>
    </div>   
    
    <% } %>
    </div>
  </div>
  
</div>
  
</div>
    <div class="container">
      <hr />
      <div class="row">
        <div class="span12">
        <p>@Twitter</p>
        <p>@FaceBook</p>
        </div>
      </div>
    </div>
  <script src="./js/jquery-1.7.2.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
    <script src="./js/charcounter.js"></script>
  <script src="./js/app.js"></script>
  
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </body>
</html>
