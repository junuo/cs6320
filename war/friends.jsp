<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreService" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.Entity" %>
<%@ page import="com.google.appengine.api.datastore.EntityNotFoundException" %>
<%@ page import="com.google.appengine.api.datastore.FetchOptions" %>
<%@ page import="com.google.appengine.api.datastore.Key" %>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>
<%@ page import="com.google.appengine.api.datastore.Query" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Friends Tweet</title>
       <meta name="author" content="Junuo Cai">
    
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	

 <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <link href="./css/bootstrap.min.css" media="all" type="text/css" rel="stylesheet">
    <link href="./css/bootstrap-responsive.min.css" media="all" type="text/css" rel="stylesheet">
    <link href="./css/font-awesome.css" rel="stylesheet" >
    <link href="./css/nav-fix.css" media="all" type="text/css" rel="stylesheet">
    

  </head>

<body>
   
   <div class="container">
      <header class="header clearfix">
        <nav>
          <ul class="nav nav-pills float-right">
            <li class="nav-item">
              <a class="nav-link active" href="home.jsp">Tweet <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="friends_tweet" href="./friends.jsp">Friends</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="friends_top_tweets" href="./top_tweets.jsp">Top Tweets</a>
            </li>
            <li class="nav-item">
            <fb:login-button size="large" autologoutlink="true" scope="public_profile,email" onlogin="checkLoginState();">
			</fb:login-button>
            </li >
          </ul>
        </nav>
        <h3 class="text-muted">Twitter on Facebook</h3>
      </header>
      
      <main role="main">
        <div class="jumbotron">
          <h1 class="display-3">Tweets of Friends</h1>
        </div>
      </main>
 </div>
  

<% Cookie[] cookies = request.getCookies();
		String name="";
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				Cookie cookie = cookies[i];
				if (cookie.getName().equals("userid")) {
					name = cookie.getValue();
					break;
				}
				else{
				}
			}
		}
		
 %> 
		
		<% 
	    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	    // Run an ancestor query to ensure we see the most up-to-date
	    // view of the Greetings belonging to the selected Guestbook.
	    Query query = new Query("Tweet");
	    query.addFilter("userid",
	            Query.FilterOperator.NOT_EQUAL,
	            name);
	    List<Entity> tweets = datastore.prepare(query).asList(FetchOptions.Builder.withChunkSize(2000));
	    int num_tweets = tweets.size();
	    if (tweets.isEmpty()) {
		%>
<div class="alert alert-danger block">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4>No Tweet, now!</h4>
</div>
<% 
} 
	     else {
	    	 for (Entity tweet : tweets) { 
	    			String tweet_text =  (String) tweet.getProperty("text");
	    			String tweet_date = (String) tweet.getProperty("date");
	    			String username = (String) tweet.getProperty("username");
	    			Long count = (Long) tweet.getProperty("count");
	    			String key = KeyFactory.keyToString(tweet.getKey()); 
					String href = "'view_tweet.jsp?tweet_key=" + key + "'";
					String picture = "'" + (String) tweet.getProperty("picture") + "'";
					%>
	     <div class="container">
        <ul class="list-group">

            <li class="list-group-item list-group-item-success">
              <p>
                <a class="active" target="_blank" href=<%=href%> ><%=tweet_text%></a>
              </p>
              <p>
                <strong>Created at: <%=tweet_date %></strong>
              </p>
              <p>
                <strong> Posted by: <%=username %></strong> 
              </p>
               <p>
                <strong> View Count : <%=count %></strong> 
              </p>
              <div id="profile_pic" class="span1"><a href=<%=href%> class="thumbnail"><img src=<%=picture%> alt=""></a></div>
            </li>


      </ul>
  </div>
	     <% }  
	    	} %>
 <script>
document.getElementById('profile_link').href = localStorage.profile_url;
</script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
 
</body>
</html>